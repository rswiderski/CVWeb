var app = angular.module("app",[]);

app.controller("MainController", function(){
    var vm = this;
    vm.title = 'Curriculum Vitae';
    vm.name = 'Thibaut HARDY';
    vm.job = 'Android Developper';
    vm.societyName = 'Infotel';
   

    vm.goTo = function(id){
        console.log("bidule")
        vm.mainContent = 'views/'+id+'.html';
    };

    vm.tabsnav =[
		{
            tabTitle: 'Home',
            url: 'home.html',
            id: 'home'

        },
        {
            tabTitle: 'Projects',
            url: 'projects.html',
            id: 'projects'
        },

        {
           	tabTitle: 'Skill',
            url: 'skill.html',
            id: 'skill'
        }
	];
    vm.tabsfoot =[
        {
            tabTitle: 'Home',
            url: 'home.html',
            id: 'home'
        },
        {
            tabTitle: 'Projects',
            url: 'Projects.html',
            id: 'Projects'
        },

        {
            tabTitle: 'Skill',
            url: 'Skill.html',
            id: 'Skill'
        }
    ];
});