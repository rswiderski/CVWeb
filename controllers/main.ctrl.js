var app = angular.module("app",[]);

app.controller("MainController", function(){
    var vm = this;
    vm.title = 'Curriculum Vitae';
    vm.name = 'Thibaut HARDY';
    vm.job = 'Android Developper';
    vm.societyName = 'Infotel';
   

    vm.goTo = function(id){
        console.log("bidule")
        vm.mainContent = id+'.html';
    };

    vm.tabsnav =[
		{
            tabTitle: 'Skills',
            url: 'skill.html',
            id: 'skill'
        },
        {
            tabTitle: 'Experience',
            url: 'Experience.html',
            id: 'Experience'
        },
        {
           	tabTitle: 'Education',
            url: 'Education.html',
            id: 'Education'
        },
        {
            tabTitle: 'Projects',
            url: 'projects.html',
            id: 'projects'
        },

        {
            tabTitle: 'Personal',
            url: 'Personal.html',
            id: 'Personal'
        }
	];
    vm.tabsfoot =[
        {
            tabTitle: 'Home',
            url: 'home.html',
            id: 'home'
        },
        {
            tabTitle: 'Projects',
            url: 'Projects.html',
            id: 'Projects'
        },

        {
            tabTitle: 'Skill',
            url: 'Skill.html',
            id: 'Skill'
        }
    ];
});